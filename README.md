# Trabajo Práctico Nro. 2
**Alumno:** Nicolás Alejandro Silva Coronel

El trabajo consiste en el desarrollo de un sistema de autenticación utilizando un proceso de integración continua para la realización de los test y en caso de éxito su posterior  despliegue en el ambiente correspondiente.

## Tecnologías Utilizadas

 - Python 3.5
 - Django Framework 2.2.1
 - Gitlab Ci
 - Heroku 

## Ambientes 

Se utilizan 3 ambientes: Desarrollo, Homologación y Produccion.

**Desarrollo:** https://tp2-gcc-develop.herokuapp.com/
**Homologación:** https://tp2-gcc-homologacion.herokuapp.com/
**Producción:** https://tp2-gcc-produccion.herokuapp.com/

## Gitlab Ci

Para el proceso de integración hemos definido 4 stages o pasos para realizar el despliegue en el ambiente correspondiente:

**Test:** Se encarga de correr los test unitarios y se ejecuta para todos los ambientes.

**Desarrollo** Se encarga de desplegar automáticamente al ambiente de Desarrollo luego de un push en la rama develop.

**Homologación** Se encarga de desplegar automáticamente al ambiente de Homologación luego de un push en la rama master.

**Producción** Se encarga de desplegar automáticamente al ambiente de Producción luego de que un tag sea creado.

En todos los ambientes primeramente se realizan los tests, y si estos son éxitosos entonces se procede al despliegue automático.

