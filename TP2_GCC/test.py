from django.test import Client, TestCase
from django.contrib.auth.models import User

class TestLogin(TestCase):
    def setUp(self):
        self.client = Client()
        usuario = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')

    def test(self):
        #test de url
        response = self.client.get('/cuenta/login/')
        self.assertEqual(response.status_code, 200)

        #test de login exitoso(redirect a /)
        response = self.client.post('/cuenta/login/',{'username': 'john', 'password': 'johnpassword'},follow=True)
        redirect_url = response.redirect_chain[-1][0]
        status_code = response.redirect_chain[-1][1]
        self.assertTrue(redirect_url == '/' and status_code == 302)

        #test de login fallido
        response = self.client.post('/cuenta/login/',{'username': 'johsn', 'password': 'johnpasssword'})
        self.assertEqual(response.status_code, 200)
